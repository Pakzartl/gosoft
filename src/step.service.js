// * TODO: Implement function for updating user's step data in store
// * TODO: Function for getting user's step data may need some adjustments
const StepTracker = require('./StepTracker');
module.exports = function stepService(store) {

    const service = {};

    service.get = (username) => {
        var trackUser = StepTracker.store = store[username];
        if (!trackUser) {
            return {
                statusCode: 404,
                error: `User doesn't exist`
            };
        }
        return trackUser;
    };

    service.add = (username, ts, newSteps) => {
        var data = store[username] = {
            ts: ts,
            cumulativeSteps: newSteps
        }
        return data;
        // Assume that `store` is initially an empty object {}. An example `store` is:
        // {
        //   jenna: {
        //     ts: 1503256778463,
        //     cumulativeSteps: 12323,
        //   },
        //   james: {
        //     ts: 1503256824767,
        //     cumulativeSteps: 587,
        //   },
        // }

    };

    // console.log(track)

    return service;
};