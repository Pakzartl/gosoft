const WebSocketServer = require('ws').Server;

module.exports = (stepService) => {
    const WEBSOCKET_PORT = 8082;

    // * TODO: Write the WebSocket API for receiving `update`s,
    //         using `stepService` for data persistence.
    //         Make sure to return an instance of a WebSocketServer.
    const wsServer = new WebSocketServer({
        port: WEBSOCKET_PORT
    });

    wsServer.on('connection', function connection(wsServer) {

    });

    return wsServer;
};