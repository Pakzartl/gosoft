// * You may uncomment one of these modules:
const express = require('express');
const stepService = require('./step.service');
const testStore = require('../spec/testStore');

// const koa = require('koa');
// const hapi = require('@hapi/hapi');
// const restify = require('restify');
const app = express();

module.exports = (stepService) => {
    const REST_PORT = 10000;
    const store = testStore();
    // var service = StepService(store);
    // * TODO: Write the GET endpoint, using `stepService` for data access
    app.get(`/users/:username/steps`, async function(req, res) {
        // var service = stepService(store);
        var user = await stepService.get(req.params.username);
        if (!user) {
            return res.send(404, {
                statusCode: 404,
                error: `User doesn't exist`
            })
        }

        return res.send(200, {Body: user});
    });

    app.listen(REST_PORT);
};